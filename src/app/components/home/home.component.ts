import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  hotels=[
    {
      name:"Leela Palace",
      imgurl:""
    },{
      name:"Hilton",
      imgurl:""
    },{
      name:"Taj",
      imgurl:""
    },{
      name:"Western",
      imgurl:""
    }
  ]

  constructor() { }

  ngOnInit(): void {
  }

  order(hotelId:any){
    console.log(" : ",hotelId);
  }

  seemenu(hotelId:any){
    console.log(" : ",hotelId);
  }

}
