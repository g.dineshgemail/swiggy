import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  swiggyLogo="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANUAAAB4CAMAAABSHEeBAAAAkFBMVEX/////hwL/hQD/ggD/gAD/fgD/eQD/9e7/fAD/+/j/nVn/+PT/vZT/+ff/dwD/7N//jSb/ijH/m0j/7+X/p3P/28T/ol7/6Nn/2L7/z7H/sHz/4M3/nFP/w5z/5NP/cwD/qmv/hx//lj//kTf/l0v/oGP/t4r/yav/kUL/aAD/yKT/r4P/rHP/pGr/0Lj/ixmoIbRjAAAHS0lEQVR4nO2ceZuiOBCHycXRsghNg4IIIl7b4/H9v93mABoV4zG7k7CP7z+jSM9TPyqpVCoJhvHmZaxJNJ1Oo9xSbci/xWg8XwVBCRFCsAyCpZuPVJv0uzjRBngEgh8g8VA6HbTP5h+QgGswWm1Um/YqzrG0YY8m7jFszwfpr8nXTU0cexWpNvF5tgjJNDF/oVS1kU8ySs07mri7slC1oU+R4QdEAYCCAcmy/MdE0VZoz1Qb+yjO570u1ZEVTFSb+yCfj3qKyyoHEeGd/TOiaN/KhpBBJc+JopnGAAJ8WDwpCoBiqtrou6wfjxQtle5dK5GL6s+h8Fy12XJGlSz3g2h16v0d6T0YJ7bUU4mVlH0/2K5qw6XINAG4pjF81essrNpwGZE0qnNVn/1NMFFtuoRUGivgh3VT1Zej2vabhEvpPFGiCvj6poO5PK2QqdI4d59LI6BUlcZpU3xnCJaogr5q42/SP8Q+pAp4qo2/iScVJVdVaJsL3knXpS3wb11Du/WiryC2Tx/aFgfHd6plXFXWqX1ChIjtmX661TeuP6YqTLOgplrGC3eaj/Se4YePtECBo2svusa5Fy1ivb1yg3stMNM2esvw5aMwgOnc7TL/3k4n2iu9kzHRCccVsPSDVO/5/XffwuJ9sJnq7LBInrPfhnyoNl3CWFphkrprq9r224zWr6pCOq9+b14o3OqvKnpZlc4FQefZ9ZBW1bdq02UsXnQW1rfERMkfWbrvoVRtuJSRvCJ4C7RQbbic+Us9y9R2JiyYla+oInpngobxykCMU90nkfIVrH7QUbXVd3khtu90TtkF26enI0PYexbunu1Zhe6xgpE86Sz0qdrih4ifk0V0Xj39IVxiKFohZNwRBVf6xwpO+GtZ+X7p+35VLf2G8kT6FGKt0/UzwnDMCZsPnN7NFrZqW3+br+vBzB5Gr5JxvXMBLodXpXackaDO8659hTQuLvUyOc7XqwoSgqEfbxLmk6/LcAFXuue154yzABOE6giPMPkyelQVmk+sLjleLmaxrY2XqvBetZlPMr6cFPep0nifTz/OZbxj042LaEF0rgL2M7tUNbr0FaxU2/gCF/var1XpXoPpZWRfq+r6D+tcW7+NezYj2dGR6VdXlT+QXP0CKzs7ppkYedX5rvGOQDn52ZiFl90jgQNtf4z0rGudhYrdEIoV/Vi39s0BO1dt228wu1F2t4fb/hhpb2UGHYY3q+ri9C7rn4bbqQTTngq1p/+Bq3tcbwcfcFD/YXfhrSGWKq7JT+eiyDAzpUvOz2+WQ8zU++ieXyf/h07F6YR3vFBtzL9HXjbD7+7/ECkatiK8D+fU/WPEvGsNdk51g5B1LVvzc8HPk6MhFsrukhRfqk34Lxj2q6XevHnz5s2bN38Cp91K4TjNCcyfC2f/Cv5q/6D7XzSXnO4nVXsWwm3s+3s+u7A2i8WBX5yLnZmbOHOZXckq+2hNjdKlX8Vup8Yefi98f3nY8mwq+oxjkdZP4ixWlAtbBxtBiBGvWaY28ri1xGMWWh4StfSNjYq/xP1OhjCEEJGy3WYWVQSxS/ZqTL+FAUIl+2C4NiwUTTDnJqCaAOQvBqCTeLhgF23+qgoXg5KXnekHT6gKSwQAIQQBVNYV6a0JAaSXILBj9n1qihddjIm6qs0SwirZ2GXCW5gP4C40RjsAgtBw1hDu+E0bDEyuyjnQBwDnx+3aQ3XxLKTzfpJtj9vKrg/drqhk6vEFfSaqagEniA6OkdQTjA19vkf+7izsssqmLZpZqyqingX8XrepCC6ohpSVZ6x93fVmBKCKrQ+hX39USoeYTm8Pbc2SPni8H2WIvxJwS7+IapLbqEoxwOcFTmt39Yozhwq1ZylVq2wylpgAIPhRV1gcKhLNeC3JPn5CFIvH36ryIVzS7mSF48lEHH3OfbGVnV2YTMRDoG6Cy6XSl2nNMfOMtxD2HwkVSZ9+xXYRA1Sv5rQtkPC3CziB53lmwU8bRCdeyogKzzTNIhb382U8qPQtdceMBjFgijFmzPcd4K0oqzenJFpViJ/Dd/gefu41por5asYLu7DefmaxzXeKDwQ6M/beskB8OTDzsDiOj5o9cm1kpw0U0vC2KoNTrSrcAbhijc4PoBgWGFssdpyoYpKyVraHoH5hKI1yvOQ3pZ3LawJzq4r+iteOMbJGcyJUGSzW0/sti44HrSrqcqRw2+A8IKfECLNWlUFjNxtfLeos0ty1aVSxq3g9pbr2qFY1KwAoFxPDyQNtVLnUNaTwaGDImitEnDz/xqSt0rJ+JdrTjMYQaBeFRz1UieSCjnEAmkVhdl6epViVsRcHTrHZZKs5Fm9Qpvlcm8DSFmjXvSTC9RIx9OrDcY7r1curxGxWwMMKqt24mgQlLFkTqrE+6mFms24XdFxcoqbvhy79A1gG2c8a/ngRlDSlDTbtXoVwVZaKD2Tl0aw7soybDz9JnDUJz+6Iovw8wDn5dHqW81nWeFjbwf8U/wCK32rssLDXsgAAAABJRU5ErkJggg==";

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  
  signin(){
    console.log("Signed In");
    this.router.navigate(['/home']);
  }
  
  register(){
    console.log("Register here");
    this.router.navigateByUrl('/register');  
  }
}
